import { Component } from "react";
import context, { Consumer } from "./context";

class Child extends Component {
    state = { name: 'child' }
    static getDerivedStateFromProps(newProps, newState) {
        console.log('111 组件将接受父组件传递来的，更新后的props', newProps, newState)
        return null
    }
    shouldComponentUpdate(newProps, newState) {
        console.log('222 判断组件是否要进行更新', newProps, newState)
        return true
    }
    render() {
        console.log('333 组件执行挂载DOM')
        let { name } = this.state
        let { parentName, changeParentName } = this.props
        return (
            <div>
                <h1>父</h1>
                <p>父级名字： {parentName}</p>
                <button
                    onClick={
                        () => {
                            changeParentName('修改父级名字' + Date.now())
                        }
                    }
                >是修改父级的名字</button>
                <h1>子</h1>
                <p>子级名字： {name}</p>
                <button
                    onClick={
                        () => {
                            this.setState({ name: '更新自己的名字' + Date.now() })
                        }
                    }
                >是修改自己的名字</button>
            </div>
        )
    }
    getSnapshotBeforeUpdate(prevProps, prevState) {
        console.log('4444 用于获取更新的DOM', prevProps, prevState)
        return { info: '要传递给componentDidUpdate的信息' }
    }
    componentDidUpdate() {
        console.log('555 组件完成了更新')
    }
}

export default Child;