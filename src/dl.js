import React, { Component } from 'react'

export default class DL extends Component {
    // state = { isOpen: true }
    render() {
        let { dlData, name, openName, changeOpen } = this.props
        // let { isOpen } = this.state
        console.log('let { dlData } = this.props: ', dlData)
        return (
            <dl className={'friend-group ' + (name === openName ? 'expanded' : '')}>
                <dt
                    // onClick={() => {
                    //     this.setState({ isOpen: !isOpen })
                    // }}
                    onClick={() => changeOpen(name)}
                >{dlData.title}</dt>
                {
                    dlData.list.map((item, index) => {
                        return <dd key={index}>{item.name}</dd>
                    })
                }
            </dl >
        )
    }
}