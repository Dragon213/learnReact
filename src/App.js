import React from 'react'
import Child from './Child'
import { Provider } from './context';

class App extends React.Component {
  state = { name: 'parent' }
  changeName = (newName) => {
    this.setState({name: newName})
  }
  render() {
    let { name } = this.state
    return (
      <div>
        <Child
          parentName = {name}
          changeParentName = {this.changeName}
        ></Child>
      </div>
    )
  }
}

export default App;